﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFiring : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float timer = 1f;
    public float fTime = 1f;
    public void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Instantiate(bulletPrefab, transform.position, transform.rotation);
            timer = fTime;

        }
    }
}
