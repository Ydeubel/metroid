﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
	public int bossHealth;
	public Transform player;
	public bool isEnraged = false;
	public bool isFlipped = false;
	public GameObject bossShoot;
	public Slider healthBar;
	public GameObject bullet;
	public void Update()
	{
		healthBar.value = bossHealth;
		if (bossHealth <= 0)
		{
			Destroy(gameObject);
		}
		Enraged();
	}

	public void LookAtPlayer()
	{
		Vector3 flipped = transform.localScale;
		flipped.z *= -1f;

		if (transform.position.x > player.position.x  && isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = false;
		}
		else if (transform.position.x < player.position.x && !isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = true;
		}
	}

	public void Enraged()
	{
		if (bossHealth <= 25 && isEnraged == false)
		{
			bullet.GetComponent<Bullet>().speed += 10;
			isEnraged = true;
			bossShoot.GetComponent<BossFiring>().fTime = 0.6f;
		}
	}
}
