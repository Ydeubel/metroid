﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sattackSkel : MonoBehaviour
{
    public int mobDamage = 1;
    public GameObject player;

    private void Start()
    {
        player = GameObject.Find("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Player1 enemy = player.GetComponent<Player1>();
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Touché");
            if (enemy != null)
            {
                enemy.TakeDamage(mobDamage);
                
            }
        }
    }
}
