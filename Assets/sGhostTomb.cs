﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class sGhostTomb : MonoBehaviour
{
    public Transform target;
    public float ghostDamage;
    public float agroRange;
    public bool isFlipped = false;

    public Rigidbody2D rb;

    public int speed;
    
    
    private Vector2 movementZone;
    // Start is called before the first frame update
    void Start()
    {
    
    
    }

    // Update is called once per frame
    void Update()
    {
        target = GameObject.FindGameObjectWithTag("Mêlée").GetComponent<Transform>();

        float distPlayer = Vector2.Distance(transform.position, target.position);

        if (distPlayer < agroRange)
        {
            ChasePlayer();
        }
        else
        {
            StopChasePlayer();
        }

        LookAtTarget();


    }

    public void LookAtTarget()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x > target.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
        else if (transform.position.x < target.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
    }

    public void ChasePlayer()
    {
        if (transform.position.x < target.position.x)
        {
            rb.velocity = new Vector2(speed, 0);
        }
        else
        {
            rb.velocity = new Vector2(-speed, 0);
        }
    }

    public void StopChasePlayer()
    {
        Vector2 pattern = new Vector2(Random.Range(-20f, 20f), Random.Range(-20f, 20f));
        rb.velocity = pattern;
    }

    
    
}
        
    
    





    
    

