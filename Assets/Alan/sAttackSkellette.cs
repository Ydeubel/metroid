﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sAttackSkellette : MonoBehaviour
{
    public int mobDamage = 1;
    public GameObject player1;
    public GameObject player2;

    private void Start()
    {
        player1 = GameObject.Find("Player");
        player2 = GameObject.Find("Player 2");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        Player1 enemy1 = player1.GetComponent<Player1>();
        Player2 enemy2 = player2.GetComponent<Player2>();
        if (other.gameObject.CompareTag("Player1"))
        {

            if (enemy1 != null)
            {
                Debug.Log("Touché p1");
                enemy1.TakeDamage(mobDamage);

            }
        }

        if (other.gameObject.CompareTag("Player2"))
        {
                
                if (enemy2 != null)
                {
                    Debug.Log("Touché p2");
                    enemy2.TakeDamage2(mobDamage);

                }

        }
        
    }
}
